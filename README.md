# **DGMS AWS-based architecture template**

## Resources:
- Multi AZ internet-facing Application Load Balancer (ALB) with listener and routing rules
- Frontend (FE) autoscaling group (with scale in and out policy), launch configuration, target group
- EFS (with Multi AZ mount target) to share resources among frontend instances
- Backend (BE) autoscaling group(with scale in and out policy), launch configuration, target group
- ElastiCache cluster (Redis)
- ALB, FE, BE, EFS, ElastiCache security groups
- MongoDB instance and security group
- EC2 instance profile

## **Prerequisites**
Before to launch the template, be sure to have the following resource available:
- Multi AZ VPC, with subnets with internet access
- EC2 Key (to be associated to EC2 instance and SSH them)
- SNS topic: to be notified for state change of the two autoscaling group
- Have access to S3 fao-configuration-template bucket
- Bastion Server Security Group: only instances with the specified Security Group will have SSH traffic allowed for the generated EC2 instances 

## Installation steps:
This reference architecture provides a CloudFormation template for deploying DGMS/CIOMS project of FAO on AWS.
Use the template in the following order:

### 1. CIOMS-EC2-instance-profile.yaml
It creates the EC2 instance profile to grant access to SSM parameters and S3 fao-configuration-template for Nginx and MongoDB configuration.

### 2. CIOMS-mongodb-template.yml
It creates the EC2 instance with MongoDB on it. It will create MongoDB users and it will seed the db with dev database.

### 3. CIOMS-migration-template.yml
It will creates and Application Load Balancer (ALB) that routes the traffic to the following autoscaling group:
- Backend: for every request to /api*
- Frontend: otherwise

Please refer to the templates themself for more documentation. 
